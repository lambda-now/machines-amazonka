# Revision history for machines-amazonka

## 0.4.0 -- 2017-02-17

* First version. Released on an unsuspecting world.
* Handles AMIs, some instance control, and spot price history.
