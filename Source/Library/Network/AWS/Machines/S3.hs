{-|
Module:             Network.AWS.Machines.S3
Description:        Machines for running through the objects in S3 buckets.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <>
Stability:          experimental
Portability:        POSIX
-}

module Network.AWS.Machines.S3 where

type ObjectSourceT m = AWSSourceT Object m
type ObjectProcessT m b = AWSProcessT m Object b

-- | Generates batches of objects from a bucket.
listObjects ∷
    ∀ (m ∷ * → *) (f ∷ * → *).
    (M m, Foldable f) ⇒ BucketName → ObjectSourceT m
listObjects b fs = foldMapBy () (listObjectsV b)
