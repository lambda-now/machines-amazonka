{-|
Module:             Network.AWS.Machines
Description:        Machines transducers for Amazonka functions.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Network.AWS.Machines (
    module Network.AWS.Machines.AWS,
    module Network.AWS.Machines.Filters,
    module Network.AWS.Machines.Images,
    module Network.AWS.Machines.Instances,
    module Network.AWS.Machines.SpotPrices
    ) where

import Network.AWS.Machines.AWS
import Network.AWS.Machines.Filters
import Network.AWS.Machines.Images
import Network.AWS.Machines.Instances
import Network.AWS.Machines.SpotPrices
